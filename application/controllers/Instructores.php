<?php
/**
 * }
 */
class Instructores extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Instructor');

  }


  public function index(){
    $this->load->view('header');
    $this->load->view('instructores/index');
    $this->load->view('footer');
  }
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('instructores/nuevo');
    $this->load->view('footer');
  }
  public function guardar(){
    $datosNuevoInstructor=array(
      "cedula_int"=>$this->input->post('cedula_int'),
      "segundo_apellido"=>$this->input->post('segundo_apellido'),
      "primer_apellido"=>$this->input->post('primer_apellido'),
      "nombre_ins"=>$this->input->post('nombre_ins'),
      "titulo_ins"=>$this->input->post('titulo_ins'),
      "telefono_ins"=>$this->input->post('telefono_ins'),
      "direccion_ins"=>$this->input->post('direccion_ins')
    );
    $this->Instructor->insertar($datosNuevoInstructor);

  }


}
